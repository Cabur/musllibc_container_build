##################################
# Author: David J. Krupa         #
# Date: 30 March 2020            #
# Product of: GDIT Black Label   #
# MuslLibC Base Image Dockerfile #
##################################

# Rather than building off an image available we will build
# the base OS image from scratch using downloaded tar file.
FROM scratch

ADD bionic-server-cloudimg-amd64-root.tar.xz /

# Make all updates, upgrades, and installs noninteractive.
RUN \ 
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN \ 
  apt-get update && \ 
  apt-get -y upgrade

# Install packages and clean.
RUN \ 
  apt-get -y install openssh-server && \ 
  apt-get -y install python3-pip && \ 
  apt-get -y install ruby-full && \ 
  apt-get -y install nim && \ 
  apt-get install musl && \ 
  apt-get -y install vim && \ 
  apt-get install tmux && \ 
  apt-get clean && \ 
  rm -rf /var/lib/apt/lists/*

# Prohibit logging in as root user.
RUN \ 
  sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config

# Allow created user ability to log in remotely with password authentication.
RUN \ 
  sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config

# Create container username, pass, home directory, and add to sudoers group.
RUN \ 
  useradd -m -d /home/#USER# -s /bin/bash -g root -G sudo -u 1000 #USER# -p "$(openssl passwd -1 #USER PASS#)"

# Edit the sudoers file to allow user to not have to ener password for using sudo.
RUN \ 
  echo '#USER#  ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Creates SSH - RSA DSA ECDSA ED25519 keys in the container to allow remote access.
RUN \ 
  su - #USER# -c "sudo ssh-keygen -A"

# Creates users working directory when logging in.
USER #USER#
WORKDIR /home/#USER#

# Creates home env variable.
ENV HOME /home/#USER#

# Sets entrypoint to start the ssh sevice and terminal.
ENTRYPOINT \ 
  sudo service ssh start && bash