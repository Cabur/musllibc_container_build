---
- name: "Playbook 1: Create Dockerfile"
  hosts: localhost
  tasks:

    - name: Create the New Dockerfile to Build Container Base Image
      shell: "touch {{ d }}"

    - name: Create Comenet Header
      shell: |
        "{{ p }}" "##################################\n" >> "{{ d }}"
        "{{ p }}" "# Author: David J. Krupa         #\n" >> "{{ d }}"                 
        "{{ p }}" "# Date: 30 March 2020            #\n" >> "{{ d }}"
        "{{ p }}" "# Product of: GDIT Black Label   #\n" >> "{{ d }}"
        "{{ p }}" "# MuslLibC Base Image Dockerfile #\n" >> "{{ d }}"
        "{{ p }}" "##################################\n\n" >> "{{ d }}"

- name: "Playbook 2: Build Ubuntu 18.04 Live Server OS image from tar.xz File"
  hosts: localhost
  tasks:

    - name: Build Base OS Image
      shell: |
        "{{ p }}" "# Rather than building off an image available we will build\n" >> "{{ d }}"
        "{{ p }}" "# the base OS image from scratch using downloaded tar file.\n" >> "{{ d }}"
        "{{ p }}" "FROM scratch\n\n" >> "{{ d }}"

    - name: Add the Tar File for the Build
      shell: |
        "{{ p }}" "ADD bionic-server-cloudimg-amd64-root.tar.xz /\n\n" >>"{{ d }}"

- name: "Playbook 3: Update and Upgrade Base OS"
  hosts: localhost
  tasks:

    - name: Make Updates,Upgrades, and Installs  Non-Interactive
      shell: |
        "{{ p }}" "# Make all updates, upgrades, and installs noninteractive.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections\n\n" >> "{{ d }}"

    - name: Apply Updates and Upgrades to OS
      shell: |
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  apt-get update && \\ \n" >> "{{ d }}" 
        "{{ p }}" "  apt-get -y upgrade\n\n" >> "{{ d }}"

- name: "Playbook 4: Install Packages and Clean"
  hosts: localhost
  tasks:
   - shell: |
       "{{ p }}" "# Install packages and clean.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get -y install openssh-server && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get -y install python3-pip && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get -y install ruby-full && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get -y install nim && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get install musl && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get -y install vim && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get install tmux && \\ \n" >> "{{ d }}"
       "{{ p }}" "  apt-get clean && \\ \n" >> "{{ d }}"
       "{{ p }}" "  rm -rf /var/lib/apt/lists/*\n\n" >> "{{ d }}"

- name: "Playbook 5: Disable Root Login"
  hosts: localhost
  tasks:
    - shell: |
        "{{ p }}" "# Prohibit logging in as root user.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config\n\n" >> "{{ d }}"

- name: "Playbook 6: Allow Password Authentication for Remote Login"
  hosts: localhost
  tasks:
    - shell: |
        "{{ p }}" "# Allow created user ability to log in remotely with password authentication.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config\n\n" >> "{{ d }}"

- name: "Playbook 7: Create User"
  hosts: localhost
  tasks:
     
    - name: Create Username, Password, Home Directory, and Add to Sudoers Group
      shell: |
        "{{ p }}" "# Create container username, pass, home directory, and add to sudoers group.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  useradd -m -d /home/{{ musl_user }} -s /bin/bash -g root -G sudo -u 1000 {{ musl_user }}" >> "{{ d }}"
        "{{ p }}" " -p \"\$(openssl passwd -1 {{ musl_user_pass }})\"\n\n" >> "{{ d }}"
      #  "{{ p }}" "\n\n" >> "{{ d }}"
      #  "{{ p }}" "  useradd -rm -d /home/{{ musl_user }} -s /bin/bash -g root -G sudo -u 1000 {{ musl_user }} #USER PASS#\n\n" >> "{{ d }}"

    - name: Allow User to Use Sudo without Entering Password 
      shell: |
        "{{ p }}" "# Edit the sudoers file to allow user to not have to ener password for using sudo.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  echo '{{ musl_user }}  ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers\n\n" >> "{{ d }}"

    - name: Create Host Keys for User 
      shell: |
        "{{ p }}" "# Creates SSH - RSA DSA ECDSA ED25519 keys in the container to allow remote access.\n" >> "{{ d }}"
        "{{ p }}" "RUN \\ \n" >> "{{ d }}"
        "{{ p }}" "  su - {{ musl_user }} -c \"sudo ssh-keygen -A\"\n\n" >> "{{ d }}"

- name: "Playbook 8: Default to User and Working Directory when Loging into Container"
  hosts: localhost
  tasks:
    - shell: |
        "{{ p }}" "# Creates users working directory when logging in.\n" >> "{{ d }}"
        "{{ p }}" "USER {{ musl_user }}\n" >> "{{ d }}"
        "{{ p }}" "WORKDIR /home/{{ musl_user }}\n\n" >> "{{ d }}"

- name: "Playbook 9: Set Environment Variables"
  hosts: localhost
  tasks:
    - shell: |
        "{{ p }}" "# Creates home env variable.\n" >> "{{ d }}"
        "{{ p }}" "ENV HOME /home/{{ musl_user }}\n\n" >> "{{ d }}"

- name: "Playbook 10: Start Services and bash shell at login"
  hosts: localhost
  tasks:
    - shell: |
        "{{ p }}" "# Sets entrypoint to start the ssh sevice and terminal.\n" >> "{{ d }}"
        "{{ p }}" "ENTRYPOINT \\ \n" >> "{{ d }}"
        "{{ p }}" "  sudo service ssh start && bash" >> "{{ d }}"

