#!/bin/bash 

# Author: David J. Kruopa
# Date: 30 March 2020
# Product of: GDIT
# Item:  MuslLibC Container

################################################################################
# This is a shell script that will determine your host system, install the     #
# appropriate packages, and youse ansible to build the Dockerfile that will    #
# be used to create a base image and then a container to that runs MuslLibC    #
# on Ubuntu with the appropriate packages. Run the Script using the command    #
# sudo ./run_musl.sh and enter your password when prompted                     # 
################################################################################

# Below is a basic if statement that checks whether you are using a RHEL based
# machine such as CentOS or Fedora, or a Debian based machine such as Ubuntu.
# Of note, This script has been tested on CentOS 7 and Ubuntu 18.04 and is
# provided for learning and training purposes.

# Before container is built, prompts for the  Musl Container Username you
# want to use.
printf "\n\n\n\n\n"
read -p 'Create New Musl Username: ' uservar

# Before container is built, prompts for the Musl Container password you
# want to use. (you must enter something)
read -sp 'Create New Musl Password for remote access: ' passvar

# Checks the condition if yum is used for packages.
if [ -n "$(command -v yum)" ]
then
  printf "\n\n\n\n\nYour system uses yum to install packages!\n\n\n\n\n"
  sleep 5

  # Installs the appropriate dependencies to set up and run the container.
  # If any of these are already installed it will do nothing and proceed.
  yum update -y
  yum upgrade -y
  yum install ansible -y
  yum install docker -y
  systemctl enable docker
  systemctl start docker

  # More for show but verifies version to check successful docker installtion.
  printf "\n\nVerify Docker Installation and Version:\n"
  docker --version
   
    echo -ne 'Progress: #########                     (33%)\r'
    sleep 2
    echo -ne 'Progress: #################             (66%)\r'
    sleep 4
    echo -ne 'Progress: ###########################   (100%)\r'
    sleep 4
    echo -ne 'Comlpete!\n'
  
  # More for show, but verifies version to check succesful ansible installation.
  printf "\n\nVerify Ansible Installation and Version:\n"
  ansible --version

    echo -ne 'Progress: ###########                   (42%)\r'
    sleep 3
    echo -ne 'Progress: ###############               (56%)\r'
    sleep 6
    echo -ne 'Progress: ###################           (72%)\r'
    sleep 6
    echo -ne 'Progress: ###########################   (100%)\r'
    sleep 3
    echo -ne 'Comlpete!\n'

  printf "\n\n"

# Checks the condition if apt is used for packages.
elif [ -n "$(command -v apt-get)" ]
then
  printf "\n\n\n\n\nYour system uses apt-get to install packages!\n\n\n\n\n"
  sleep 5
  
  # Installs the appropriate dependencies to set up and run the container.
  # If any of these are already installed it will do nothing and proceed.
  apt-get update
  apt-get upgrade -y
  apt-get install vim -y
  apt-get install ansible -y
  snap install docker
  systemctl enable docker
  systemctl start docker
 
  # More for show, but verifies version to check succesful docker installation.
  printf "\n\nVerify Docker Installation and Version:\n"
  docker --version

    echo -ne 'Progress: #########                     (33%)\r'
    sleep 2
    echo -ne 'Progress: #################             (66%)\r'
    sleep 4
    echo -ne 'Progress: ###########################   (100%)\r'
    sleep 1
    echo -ne 'Comlpete!\n'

  # More for show, but verifies version to check succesful ansible installation.
  printf "\n\nVerify Ansible Installation and Version:\n"
  ansible --version

    echo -ne 'Progress: ###########                   (42%)\r'
    sleep 3
    echo -ne 'Progress: ###############               (56%)\r'
    sleep 6
    echo -ne 'Progress: ###################           (72%)\r'
    sleep 6
    echo -ne 'Progress: ###########################   (100%)\r'
    sleep 1
    echo -ne 'Comlpete!\n'

  printf "\n\n"
else
  echo "You have something that hasen't been tested with this script."
fi

# Before Dockerfile is built, checks for running container, stops it,
# removes container, and removes base image  if it exists. This is good
#  practice when automating clean builds.
if [[ $(docker inspect -f '{{.State.Running}}' musl_container) = true ]];
then
  docker stop musl_container
  docker rm musl_container
  docker image rm musl_base:latest
else
  docker rm musl_container
  docker image rm musl_base:latest
fi

# Removes old inventory file
rm -f musl-container-inventory

# Removes old Dockerfile
rm -f Dockerfile

# Creates the inventory file and inputs the variables for ansible playbpook
# that will build the Dockerfile.
touch musl-container-inventory
  printf "[all:vars]\n" >> musl-container-inventory
  printf "p=printf\n" >> musl-container-inventory
  printf "d=Dockerfile\n" >> musl-container-inventory
  printf "musl_user=#USER#\n" >> musl-container-inventory
  printf "musl_user_pass=#USER PASS#\n" >> musl-container-inventory

# This command appends the username in the Dockerfile and sets up home
# your container home directory and env variables appropriately.
sed -i 's/#USER#/'$uservar'/g' musl-container-inventory

# This command appends the password in the Dockerfile and sets up home
# your container home directory and env variables appropriately. Of note,
# it is never good practice to hardcode a password. After the script ends,
# the password variable will be reset in both the script and the Dockerfile.
sed -i 's/#USER PASS#/'$passvar'/g' musl-container-inventory

# Runs ansible playbook to build the Dockerfile that will create base image.
ansible-playbook -i musl-container-inventory build-dockerfile.yml

# This command builds the container base image with tags
docker build -t musl_base:latest .

# This command builds and names the container from the base image and starts it,
docker run -it -d --name musl_container musl_base:latest
sleep 5

# Sets up a variable that stores the container ip address for remote log in and
# displays on the screen.
container_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' musl_container)
  printf "\n\n\n\n\n"
  echo "##########################################################################"
  echo "You are now connected to the Musl Container!"
  printf "\n\n\nYour container ip for remote access is: $container_ip"
  printf "\n\n"
  echo "Try opening another terminal on this host and ssh with the following command:"
 printf "\n\n"
 echo "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o \"LogLevel ERROR\" $uservar@$container_ip"
  printf "\n\nwith the password you created earlier.\n\n\n\n"
  echo "#########################################################################"
  printf "\n\n\n\n\n"

# This command resets the container ip variable to null,
container_ip=""

# This commands opens up the bash terminal to interact with the container,
docker attach musl_container

# This command resetss to default #USER# for username var back to the inventory
# file and Dockerfile.
sed -i 's/'$uservar'/#USER#/g' musl-container-inventory
sed -i 's/'$uservar'/#USER#/g' Dockerfile

# This command resets default #USER PASS# for user password  back to the inventory
# file and Dockerfile.
sed -i 's/'$passvar'/#USER PASS#/g' musl-container-inventory
sed -i 's/'$passvar'/#USER PASS#/g' Dockerfile

